stages:
  - Compliance
  - Build
  - Test

variables:
  IMAGE_BASE: ${CI_REGISTRY}/kwinft/ci-images/archlinux/kwinft-base

workflow:
  rules:
    - when: always


Message lint:
  stage: Compliance
  image: node:latest
  rules:
    - if: '$CI_SCHED_COVERITY_SCAN'
      when: never
    - if: $CI_MERGE_REQUEST_IID
      when: always
    - if: '$CI_COMMIT_BRANCH == "master" || $CI_COMMIT_BRANCH =~ /^Plasma\// || $CI_COMMIT_TAG'
      when: never
    - when: always
  variables:
    UPSTREAM: https://${CI_REGISTRY_USER}:${CI_REGISTRY_PASSWORD}@${CI_SERVER_HOST}/kwinft/kwinft.git
  script:
    - if [ -n "$CI_MERGE_REQUEST_TARGET_BRANCH_NAME" ];
      then export COMPARE_BRANCH=$CI_MERGE_REQUEST_TARGET_BRANCH_NAME; else export COMPARE_BRANCH=master; fi
    - "echo Branch to compare: $COMPARE_BRANCH"
    - yarn global add @commitlint/cli
    - yarn add conventional-changelog-conventionalcommits
    - git remote add _upstream $UPSTREAM || git remote set-url _upstream $UPSTREAM
    - git fetch -q _upstream $COMPARE_BRANCH
    - commitlint --verbose --config=ci/compliance/commitlint.config.js --from=_upstream/$COMPARE_BRANCH
  cache:
    paths:
      - node_modules/


Coverity Scan:
  stage: Build
  image: ${IMAGE_BASE}-master:latest
  rules:
    - if: '$CI_SCHED_COVERITY_SCAN'
      when: always
    - when: never
  script:
  - mkdir ci-build && cd ci-build
  - curl -o /tmp/cov-analysis-linux64.tgz https://scan.coverity.com/download/linux64
    --form project=$COVERITY_SCAN_PROJECT_NAME --form token=$COVERITY_SCAN_TOKEN
  - tar xfz /tmp/cov-analysis-linux64.tgz
  - cmake -DCMAKE_CXX_COMPILER=clang++ -G Ninja ../
  - cov-analysis-linux64-*/bin/cov-configure --comptype clangcc --compiler /usr/bin/clang++
  - cov-analysis-linux64-*/bin/cov-build --dir cov-int ninja -j$(nproc)
  - tail cov-int/build-log.txt
  - tar cfz cov-int.tar.gz cov-int
  - VERSION="${CI_COMMIT_REF_NAME}-${CI_COMMIT_SHORT_SHA}"
  - 'echo "Nightly Scan: $VERSION / $CI_COMMIT_TITLE / $CI_PIPELINE_ID"'
  - 'curl https://scan.coverity.com/builds?project=$COVERITY_SCAN_PROJECT_NAME
    --form token=$COVERITY_SCAN_TOKEN --form email=$GITLAB_USER_EMAIL
    --form file=@cov-int.tar.gz --form version="$VERSION"
    --form description="Nightly Scan: $VERSION / $CI_COMMIT_TITLE / $CI_PIPELINE_ID"'


.common-master: &common-master
  image: ${IMAGE_BASE}-master:latest
  rules:
    - if: '$CI_SCHED_COVERITY_SCAN'
      when: never
    - if: '$CI_COMMIT_BRANCH =~ /^Plasma\// || $CI_COMMIT_TAG'
      when: never
    - when: on_success

.common-stable: &common-stable
  image: ${IMAGE_BASE}-stable:latest
  rules:
    - if: '$CI_SCHED_COVERITY_SCAN'
      when: never
    - if: '$CI_COMMIT_BRANCH =~ /^Plasma\// || $CI_COMMIT_TAG'
      when: on_success
    - when: never


.common-build: &common-build
  stage: Build
  script:
    - mkdir ci-build && cd ci-build
    - cmake -DCMAKE_INSTALL_PREFIX:PATH=/usr ../
    - make -j$(nproc)
    - make install -j$(nproc)
  artifacts:
    paths:
      - ci-build
    expire_in: 1 week

Master build:
  <<: *common-build
  <<: *common-master

Stable build:
  <<: *common-build
  <<: *common-stable


.common-test: &common-test
  stage: Test
  script:
    - cd ci-build
    - Xvfb :1 -ac -screen 0 1920x1080x24 > /dev/null 2>&1 &
    - export DISPLAY=:1
    - export WAYLAND_DEBUG=1 MESA_DEBUG=1 LIBGL_DEBUG=verbose
    - export QT_LOGGING_RULES="*=true"
    - ctest -N
    # Tests currently can only run in one thread.
    # The lock screen test must be excluded for now because it is very unstable.
    # See: https://bugreports.qt.io/browse/QTBUG-82911
    - dbus-run-session ctest --output-on-failure -E kwin-testLockScreen

Master autotests:
  needs:
    - job: Master build
      artifacts: true
  <<: *common-test
  <<: *common-master

Stable autotests:
  needs:
    - job: Stable build
      artifacts: true
  <<: *common-test
  <<: *common-stable
